import axios from 'axios'

export const getProducts = ({commit}) => {
  return axios.get('http://vuex-cart-api.test/api/products').then((response) => {
    commit('setProducts', response.data)
    return Promise.resolve()
  })
}

export const addProductToCart = ({commit, dispatch}, { product, quantity }) => {
  commit('appendToCart', { product, quantity })

  dispatch('flashMessage', 'Item added to cart', { root: true })

  // api
  return axios.post('http://vuex-cart-api.test/api/cart', {
    product_id: product.id,
    quantity
  })
}

export const getCart = ({ commit }) => {
  return axios.get('http://vuex-cart-api.test/api/cart').then((response) => {
    commit('setCart', response.data)
    return Promise.resolve()
  })
}

export const removeProductFromCart = ({commit}, productId) => {
  commit('removeFromCart', productId)
  return axios.delete('http://vuex-cart-api.test/api/cart/' + productId)
}

export const removeAllProductFromCart = ({commit, dispatch}) => {
  commit('clearCart')
  dispatch('flashMessage', 'All item removed', { root: true })
  return axios.delete('http://vuex-cart-api.test/api/cart')
}
